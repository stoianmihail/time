-- Definition of time in Haskell
data Moment = Zero | Next Moment

time :: Moment -> [Moment]
time present = present : time (Next present) 
